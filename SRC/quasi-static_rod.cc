/*
 *
 *  MUSCOD-II/MC2_TEST/SRC/reentry.c
 *  (c) Daniel B. Leineweber, 1995
 *
 *  reentry of Apollo type vehicle (Plitt, 1981; Stoer/Bulirsch, 1992)
 *
 *  $Id: reentry.cpp 725 2011-08-10 23:04:16Z ckirches $
 *
 */

#include <cmath>
#include <vector>

#include "def_usrmod.hpp"

#include <Eigen/Dense>
typedef Eigen::VectorXd VectorNd;

#include <iostream>
#include <fstream>

const int NMOS   = 1; /* Number of phases (MOdel Stages) */
const int NP     = 0; /* Number of parameters */ 
const int NRC    = 0; /* Number of coupled constraints */
const int NRCE   = 0; /* Number of coupled equality constraints */

const int NXD    = 3; /* Number of differential states */
const int NXA    = 0; /* Number of algebraic states */
const int NU     = 1; /* Number of controls */
const int NPR    = 0; /* Number of local parameters */

#define  NRD_S  3
#define  NRDE_S 3

#define  NRD_E  3
#define  NRDE_E 3

std::string parameters_file = "";
std::string result_file  = "RES/result.csv";

bool gWriteResult;

static void lfcn(double *t, double *xd, double *xa, double *u,
		double *p, double *lval, double * rwh, long *iwh, InfoPtr *info)
{
	*lval = u[0] * u[0];
}

static void ffcn(double *t, double *xd, double *xa, double *u,
		double *p, double *rhs, double *rwh, long *iwh, InfoPtr *info)
{
	rhs[0] = cos( xd[2] );
	rhs[1] = sin( xd[2] );
	rhs[2] = u[0];
}

static void rdfcn_s(double *ts, double *sd, double *sa, double *u,
		double *p, double *pr, double *res, long *dpnd, InfoPtr *info)
{
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, 0, 0, 0);
		return;
	}

	res[0] = sd[0];
	res[1] = sd[1];
	res[2] = sd[2];
}

static void rdfcn_e(double *ts, double *sd, double *sa, double *u,
		double *p, double *pr, double *res, long *dpnd, InfoPtr *info)
{
	if (*dpnd) {
		*dpnd = RFCN_DPND(0, *sd, 0, 0, 0, 0);
		return;
	}

	res[0] = sd[0] - 0.8769;
	res[1] = sd[1] - 0.4133;
	res[2] = sd[2] - 0.8602;
}

std::vector< double > t_values;
std::vector< VectorNd > sd_values;
std::vector< VectorNd > u_values;

/* Save data information */
static void mplo( double *t, double *sd, double *sa, double *u, double *p, double *rwh, long *iwh, InfoPtr *info )
{
	if ( *t == 0. ) 
	{
		t_values.clear();
		sd_values.clear();
		u_values.clear();
	}

	/* Save t */
	t_values.push_back (*t);

	/* Save differential state */
	VectorNd sd_vector (NXD);
	for ( unsigned int i = 0; i < NXD; i++ )
		sd_vector[i] = sd[i];
	sd_values.push_back ( sd_vector );

	/* Save control */
	VectorNd u_vector (NU);
	for ( unsigned int i = 0; i < NU; i++ )
		u_vector[i] = u[i];
	u_values.push_back ( u_vector );

}

/* Write data information at the end of optimization process */
static void data_out( double *t, double *sd, double *sa, double *u, double *p, double *rwh, long *iwh, InfoPtr *info )
{
	if (! gWriteResult )
	{
		std::ofstream csv_stream;

		/* Open the result csv file */
		csv_stream.open ( result_file.c_str(), std::ios_base::trunc );

		if (! csv_stream )
		{
			std::cerr << " Error opening file " << result_file << std::endl;
			abort();	
		}

		/* Write column names */
		csv_stream << "COLUMNS:" << std::endl;
		csv_stream << "Curvilinear abscissa";
		for ( unsigned int i = 0; i < NXD; i++)
			csv_stream << ", " << "q" << i+1 << "(t)"; // name of state variables
		for ( unsigned int i = 0; i < NU; i++)
			csv_stream << ", " << "u" << i+1 << "(t)"; // name of control variables
		csv_stream << std::endl;
		
		/* Write data */
		csv_stream << "DATA:" << std::endl;

		if ( t_values.size() > 0 )
		{
			for ( unsigned int i = 0; i < t_values.size(); i++)
			{
				csv_stream << t_values[i];

				for ( unsigned int j = 0; j < sd_values[i].size(); j++ )
				{
					csv_stream << ", " << sd_values[i][j];	
				}	

				for ( unsigned int j = 0; j < u_values[i].size(); j++ )
				{
					csv_stream << ", " << u_values[i][j];	
				}	

				csv_stream << std::endl;
			}
		}

		csv_stream.close();
		gWriteResult = true;

	}
}

static void mout
(
 long   *imos,      ///< index of model stage (I)
 long   *imsn,      ///< index of m.s. node on current model stage (I)
 double *ts,        ///< time at m.s. node (I)
 double *te,        ///< time at end of m.s. interval (I)
 double *sd,        ///< differential states at m.s. node (I)
 double *sa,        ///< algebraic states at m.s. node (I)
 double *u,         ///< controls at m.s. node (I)
 double *udot,      ///< control slopes at m.s. node (I)
 double *ue,        ///< controls at end of m.s. interval (I)
 double *uedot,     ///< control slopes at end of m.s. interval (I)
 double *p,         ///< global model parameters (I)
 double *pr,        ///< local i.p.c. parameters (I)
 double *ccxd,
 double *mul_ccxd,  ///< multipliers of continuity conditions (I)
#if defined(PRSQP) || defined(EXTPRSQP)
 double *ares,
 double *mul_ares,
#endif
 double *rd,
 double *mul_rd,    ///< multipliers of decoupled i.p.c. (I)
 double *rc,
 double *mul_rc,    ///< multipliers of coupled i.p.c. (I)
 double *obj,
 double *rwh,       ///< real work array (I)
 long   *iwh        ///< integer work array (I)
 ) 
{
	data_out( ts, sd, sa, u, p, rwh, iwh, NULL);
}

extern "C" void def_model(void);
void def_model(void)
{
	def_mdims(NMOS, NP, NRC, NRCE);
	def_mstage(
			0,
			NXD, NXA, NU,
			NULL, lfcn,
			0, 0, 0, NULL, ffcn, NULL,
			NULL, NULL
			);
	def_mpc(0, "Start Point", NPR, NRD_S, NRDE_S, rdfcn_s, NULL);
	def_mpc(0, "End Point", NPR, NRD_E, NRDE_E, rdfcn_e, NULL);

	def_mio (NULL, mout, mplo);
}


