#!/usr/bin/python
import csv
from decimal import Decimal 
import numpy as np
import matplotlib.pyplot as plt

def readcsv( filename ):
	print "Opening " + filename

	fid = csv.reader( open( filename, "rb" ) )
	
	return fid

filename = "result.csv"
path = "../RES/"
csvfile = readcsv( path + filename )
# Define number of states, differential states and commands
NT = 1
NXD = 3
NXDDOT = 0
NU = 1

def extract_data ( fid ):
	fid_as_list = list( csvfile )
	begin_index = 3;
	num_rows = len( fid_as_list )

	data = np.array( fid_as_list[begin_index:][:] )
	data = data.astype(np.float)

	if NT > 0:
		t = data[:, range(NT) ]
	else:
		t = np.zeros( (0,0) )
	
	if NXD > 0:
		xd = data[:, range(NT, NT + NXD) ]
	else:
		xd = np.zeros( (0,0) )

	if NXDDOT > 0:
		xddot = data[:, range(NT + NXD, NT + NXD + NXDDOT) ]
	else:
		xddot = np.zeros( (0,0) )

	if NU > 0:
		u = data[:, range(NT + NXD + NXDDOT, NT + NXD + NXDDOT + NU) ]
	else:
		u = np.zeros( (0,0) )
	return t,xd, xddot, u

def enter_axes(event):
	print('enter_axes', event.inaxes)
	event.inaxes.patch.set_facecolor('yellow')
	event.canvas.draw()

def leave_axes(event):
	print('leave_axes', event.inaxes)
	event.inaxes.patch.set_facecolor('white')
	event.canvas.draw()

def enter_figure(event):
	print('enter_figure', event.canvas.figure)
	event.canvas.figure.patch.set_facecolor('red')
	event.canvas.draw()

def leave_figure(event):
	print('leave_figure', event.canvas.figure)
	event.canvas.figure.patch.set_facecolor('grey')
	event.canvas.draw()

def set_axis_legend(ax, xlabel, ylabel, title, grid = 1):
	ax.set_xlabel(xlabel)
	ax.set_ylabel(ylabel)
	ax.set_title(title)
	ax.axis('equal')
	if grid:
		ax.grid()

def plot_data( t, xd, xddot, u ):
	fig1 = plt.figure(1)

	fig1_ax1 = fig1.add_subplot(2,2,1)
	fig1_ax1.plot( t[:,0], xd[:,0])
	set_axis_legend( fig1_ax1, 't', 'q1', '')

	fig1_ax2 = fig1.add_subplot(2,2,2)
	fig1_ax2.plot( t[:,0], xd[:,1])
	set_axis_legend( fig1_ax2, 't', 'q2', '')

	fig1_ax3 = fig1.add_subplot(2,2,3)
	fig1_ax3.plot( t[:,0], xd[:,2])
	set_axis_legend( fig1_ax3, 't', 'q3', '')
	
	fig1_ax4 = fig1.add_subplot(2,2,4)
	fig1_ax4.plot( t[:,0], u[:,0])
	set_axis_legend( fig1_ax4, 't', 'u', '')

	fig2 = plt.figure(2)
	fig2_ax = fig2.add_subplot(111)
	fig2_ax.plot( xd[:,0], xd[:,1] )
	set_axis_legend( fig2_ax, 'q1', 'q2', 'quasi-static rod')
	fig2_ax.axis('equal')

	plt.show()

t, xd, xddot, u = extract_data( csvfile )
plot_data( t, xd , xddot, u )
